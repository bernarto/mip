/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.search;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import static org.apache.lucene.index.IndexWriterConfig.OpenMode.CREATE_OR_APPEND;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author bernarto
 */
public class OutletIndexer {

    public OutletIndexer(Path dir) {
        _indexDir = dir;
    }


    public OutletIndexer() {
        String dirstr = Config._idxDir;
        if (dirstr != null && dirstr.length() > 0) {
            _indexDir = Paths.get(dirstr + File.separator + "resto");
        }
    }
    private Path _indexDir;


    public void addToIndex(Long id, String resto, String hours, String dishes)
            throws IOException {

        if (id == null) {
            return;
        }
        final String empty = new String();
        resto = (resto == null) ? empty : resto;
        hours = (hours == null) ? empty : hours;
        dishes = (dishes == null) ? empty : dishes;

        IndexWriter indexWriter = null;
        try {
            Directory dir = FSDirectory.open(_indexDir);
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(CREATE_OR_APPEND);
            indexWriter = new IndexWriter(dir, iwc);

            Document doc = new Document();
            doc.add( new LongField( "idlong", id, Field.Store.NO));
            doc.add( new StringField("id", id.toString(), Field.Store.YES));
            doc.add( new TextField( "resto", new StringReader(resto)) );
            doc.add( new TextField( "hours", new StringReader(hours)) );
            doc.add( new TextField( "dishes", new StringReader(dishes)) );

            indexWriter.addDocument(doc);
        }
        finally {
            if (indexWriter != null) {
                boolean closed = false;
                int counter = 0;
                do {
                    try {
                        indexWriter.close();
                        closed = true;
                    }
                    catch (Exception e) {
                        counter++;
                        if (counter >= 10) {
                            throw e;
                        }
                    }
                }
                while(!closed);
            }
        }
    }


    public void updateInIndex(Long id, String resto, String hours, String dishes)
            throws IOException {

        if (id == null) {
            return;
        }
        final String empty = new String();
        resto = (resto == null) ? empty : resto;
        hours = (hours == null) ? empty : hours;
        dishes = (dishes == null) ? empty : dishes;

        IndexWriter indexWriter = null;
        try {
            Directory dir = FSDirectory.open(_indexDir);
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(CREATE_OR_APPEND);
            indexWriter = new IndexWriter(dir, iwc);

            Document doc = new Document();
            doc.add( new LongField( "idlong", id, Field.Store.NO));
            doc.add( new StringField("id", id.toString(), Field.Store.YES));
            doc.add( new TextField( "resto", new StringReader(resto)) );
            doc.add( new TextField( "hours", new StringReader(hours)) );
            doc.add( new TextField( "dishes", new StringReader(dishes)) );

            indexWriter.updateDocument( new Term("id", id.toString()), doc);
        }
        finally {
            if (indexWriter != null) {
                boolean closed = false;
                int counter = 0;
                do {
                    try {
                        indexWriter.close();
                        closed = true;
                    }
                    catch (Exception e) {
                        counter++;
                        if (counter >= 10) {
                            throw e;
                        }
                    }
                }
                while(!closed);
            }
        }
    }


    public void deleteFromIndex(Long id) throws IOException {
        IndexWriter indexWriter = null;
        try {
            Directory dir = FSDirectory.open(_indexDir);
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(CREATE_OR_APPEND);
            indexWriter = new IndexWriter(dir, iwc);
            indexWriter.deleteDocuments(new Term("idlong", id.toString()) );
        }
        finally {
            if (indexWriter != null) {
                boolean closed = false;
                int counter = 0;
                do {
                    try {
                        indexWriter.close();
                        closed = true;
                    }
                    catch (Exception e) {
                        counter++;
                        if (counter >= 10) {
                            throw e;
                        }
                    }
                }
                while(!closed);
            }
        }
    }

}
