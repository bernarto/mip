/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.search;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author bernarto
 */
public class OutletSearcher {

    public OutletSearcher(Path dir) {
        _indexDir = dir;
    }


    public OutletSearcher() {
        String dirstr = Config._idxDir;
        if (dirstr != null && dirstr.length() > 0) {
            _indexDir = Paths.get(dirstr + File.separator + "resto");
        }
    }
    private Path _indexDir;



    public Long[] search(String line) throws IOException {

        Long[] result = new Long [0];
        IndexReader reader = null;
        try {
            reader = DirectoryReader.open( FSDirectory.open( _indexDir) );
            IndexSearcher searcher = new IndexSearcher( reader);
            Analyzer analyzer = new StandardAnalyzer();

            String[] fields = new String[] {"id", "resto", "hours", "dishes"};
            if (line.indexOf('"') >= 0) {
                fields = new String[] {"resto", "hours", "dishes"};
            }

            MultiFieldQueryParser parser =
                    new MultiFieldQueryParser( fields, analyzer);
            Query query = parser.parse(line);
            TopDocs top = searcher.search(query, 3000);
            ScoreDoc[] hits = top.scoreDocs;
            if (hits != null && hits.length > 0) {

                result = new Long [ hits.length ];
                int idx = 0;
                for (ScoreDoc hit : hits) {

                    Document doc = searcher.doc(hit.doc);
                    String idstr = doc.get("id");
                    result[idx++] = Long.parseLong(idstr);
                }
            }
        }
        catch (ParseException pe) {            
        }
        finally {
            if (reader != null) {
                reader.close();
            }
        }
        return result;
    }
    
}
