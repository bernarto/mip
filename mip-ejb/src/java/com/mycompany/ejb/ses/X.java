/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ses;


import com.mycompany.ejb.ent.*;
import java.math.BigDecimal;

import java.util.Collection;

import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Date;

import javax.json.JsonBuilderFactory;
import javax.json.JsonObjectBuilder;
import javax.json.JsonArrayBuilder;

/**
 *
 * @author bernarto
 */
public class X {

// User
    public static JsonObjectBuilder e2j( User e, JsonBuilderFactory f, Long uid, Long rid ) {
        JsonObjectBuilder result = null;
        if (e == null || f == null) {
            return result;
        }
        result = f.createObjectBuilder();

        if (e.getId() != null) {
            result.add("id", e.getId());
        }
        else {
            result.addNull("id");
        }
        result.add("name", e.getNama());

        String xs = String.format("%11.6f", e.getX()).trim();
        String ys = String.format("%11.6f", e.getY()).trim();
        result.add("location", xs + "," + ys);

        String bs = String.format("%12.2f", e.getBal()).trim();
        result.add("balance", bs);

        Collection<Purchase> purchases =
            (uid != null && !uid.equals( e.getId() )) ? null : e.getPurchases();
        if (purchases != null) {
            JsonArrayBuilder pab = f.createArrayBuilder();
            for (Purchase purchase : purchases) {
                if (purchase == null) {
                    continue;
                }
                if (rid != null && !rid.equals( purchase.getResto().getId() )) {
                    continue;
                }
                JsonObjectBuilder pob = e2j(purchase, f);
                if (pob != null) {
                    pab.add(pob);
                }
            }
            result.add("purchases", pab);
        }
        return result;
    }


// Purchase
    public static JsonObjectBuilder e2j( Purchase e, JsonBuilderFactory f ) {
        JsonObjectBuilder result = null;
        if (e == null || f == null) {
            return result;
        }
        result = f.createObjectBuilder();
        if (e.getDish() != null) {
            result.add("dish", e.getDish());
        }
        else {
            result.addNull("dish");
        }

        if (e.getResto() != null) {
            result.add("restaurant_name", e.getResto().getNama());
        }
        else {
            result.addNull("restaurant_name");
        }

        result.add("amount", e.getAmount());

        if (e.getTstamp() != null) {
            result.add("date", sdf4().format(e.getTstamp()) );
        }
        else {
            result.addNull("date");
        }
        return result;
    }



// Outlet
    public static JsonObjectBuilder e2j( Outlet e, JsonBuilderFactory f ) {
        JsonObjectBuilder result = null;
        if (e == null || f == null) {
            return result;
        }
        result = f.createObjectBuilder();

        if (e.getId() != null) {
            result.add("id", e.getId());
        }
        else {
            result.addNull("id");
        }
        String name = (e.getResto() != null)? e.getResto().getNama() : null;
        if (name != null) {
            result.add("name", name);
        }
        else {
            result.addNull("name");
        }

        String xs = String.format("%11.6f", e.getX()).trim();
        String ys = String.format("%11.6f", e.getY()).trim();
        result.add("location", xs + "," + ys);

        String bs = String.format("%12.2f", e.getBal()).trim();
        result.add("balance", bs);

        result.add("business_hours", composeBusinessHours(e).toString() );

        Collection<MenuItem> items = e.getItems();
        if (items != null) {
            JsonArrayBuilder ab = f.createArrayBuilder();
            for (MenuItem item : items) {
                if (item == null) {
                    continue;
                }
                JsonObjectBuilder ob = e2j(item, f);
                if (ob != null) {
                    ab.add(ob);
                }
            }
            result.add("menu", ab);
        }
        return result;
    }



// MenuItem
    public static JsonObjectBuilder e2j( MenuItem e, JsonBuilderFactory f ) {
        JsonObjectBuilder result = null;
        if (e == null || f == null) {
            return result;
        }
        result = f.createObjectBuilder();
        if (e.getNama() != null) {
            result.add("name", e.getNama());
        }
        else {
            result.addNull("name");
        }
        result.add("price", e.getPrice());
        return result;
    }



// Resto
    public static JsonObjectBuilder e2j( Resto e, JsonBuilderFactory f ) {
        JsonObjectBuilder result = null;
        if (e == null || f == null) {
            return result;
        }
        result = f.createObjectBuilder();

        if (e.getId() != null) {
            result.add("id", e.getId());
        }
        else {
            result.addNull("id");
        }
        if (e.getNama() != null) {
            result.add("name", e.getNama());
        }
        else {
            result.addNull("name");
        }
        return result;
    }


////////////////////////////////////////////////////////////////////////////////
// Helpers

    private static StringBuilder composeBusinessHours(Outlet e) {
        StringBuilder sb = new StringBuilder();
        if (e.getSunh() - e.getSunl() > 0L) {
            Date l = new Date( e.getSunl() );
            Date h = new Date( e.getSunh() );
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("Sun: ").append(sdf().format(l)).append(" - ").append(sdf().format(h));
        }
        if (e.getMonh() - e.getMonl() > 0L) {
            Date l = new Date( e.getMonl() );
            Date h = new Date( e.getMonh() );
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("Mon: ").append(sdf().format(l)).append(" - ").append(sdf().format(h));
        }
        if (e.getTueh() - e.getTuel() > 0L) {
            Date l = new Date( e.getTuel() );
            Date h = new Date( e.getTueh() );
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("Tue: ").append(sdf().format(l)).append(" - ").append(sdf().format(h));
        }
        if (e.getWedh() - e.getWedl() > 0L) {
            Date l = new Date( e.getWedl() );
            Date h = new Date( e.getWedh() );
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("Wed: ").append(sdf().format(l)).append(" - ").append(sdf().format(h));
        }
        if (e.getThuh() - e.getThul() > 0L) {
            Date l = new Date( e.getThul() );
            Date h = new Date( e.getThuh() );
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("Thu: ").append(sdf().format(l)).append(" - ").append(sdf().format(h));
        }
        if (e.getFrih() - e.getFril() > 0L) {
            Date l = new Date( e.getFril() );
            Date h = new Date( e.getFrih() );
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("Fri: ").append(sdf().format(l)).append(" - ").append(sdf().format(h));
        }
        if (e.getSath() - e.getSatl() > 0L) {
            Date l = new Date( e.getSatl() );
            Date h = new Date( e.getSath() );
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("Sat: ").append(sdf().format(l)).append(" - ").append(sdf().format(h));
        }
        return sb;
    }



    public static TimeZone getTimeZone() {
        if (_tz == null) {
            _tz = TimeZone.getTimeZone("GMT+08");
        }
        return _tz;
    }
    private static TimeZone _tz;



    private static SimpleDateFormat sdf() {
        if (_sdf == null) {
            _sdf = new SimpleDateFormat("h:mm a");
            _sdf.setTimeZone(getTimeZone());
        }
        return _sdf;
    }
    private static SimpleDateFormat _sdf;



    public static SimpleDateFormat sdf4() {
        if (_sdf4 == null) {
            _sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            _sdf4.setTimeZone(getTimeZone());
        }
        return _sdf4;
    }
    private static SimpleDateFormat _sdf4;
}
