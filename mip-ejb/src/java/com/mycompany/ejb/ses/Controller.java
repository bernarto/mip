/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ses;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mycompany.ejb.search.*;
import com.mycompany.ejb.ent.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.util.Collection;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;


import javax.persistence.Query;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;

import java.text.SimpleDateFormat;
import java.text.ParseException;

import java.util.Calendar;

import java.util.Date;
import java.util.TimeZone;

import javax.json.Json;
import javax.json.JsonWriter;
import javax.json.JsonBuilderFactory;

import java.io.StringWriter;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;

/**
 *
 * @author bernarto
 */
@Stateless
public class Controller implements ControllerRemote {

    @Resource
    private SessionContext _context;

    @PersistenceContext(unitName = "mip-pu")
    private EntityManager _em;



    @AccessTimeout(value=2, unit=TimeUnit.MINUTES)
    @Lock(LockType.WRITE)
    @Override
    public String doPurchase(Long oid, String dish, Long uid, Long rid) {
        if (oid == null || dish == null || uid == null) {
            return null;
        }
        User user = _em.find(User.class, uid);
        if (user == null) {
            throw new IllegalArgumentException("no such user");
        }
        Outlet outlet = _em.find(Outlet.class, oid);
        if (outlet == null) {
            throw new IllegalArgumentException("no such outlet");
        }
        MenuItem theItem = null;
        Collection<MenuItem> items = outlet.getItems();
        if (items != null) {
            for (MenuItem item : items) {
                if (dish.equals( item.getNama() )) {
                    theItem = item;
                    break;
                }                
            }
        }
        if (theItem == null) {
            throw new IllegalArgumentException("item not found");
        }
        if (user.getBal() < theItem.getPrice()) {
            throw new IllegalArgumentException("user balance not enough");
        }
        Purchase purchase = new Purchase();
        purchase.setDish(theItem.getNama());
        purchase.setResto(outlet.getResto());
        purchase.setAmount( theItem.getPrice() );
        purchase.setTstamp( new Date() );
        Collection<Purchase> purchases = user.getPurchases();
        if (purchases == null) {
            purchases = new ArrayList<>();
        }
        purchases.add(purchase);
        user.setPurchases(purchases);
        user.setBal( user.getBal() - purchase.getAmount() );
        _em.merge(user);

        outlet.setBal( outlet.getBal() + purchase.getAmount() );
        _em.merge(outlet);

        JsonObjectBuilder ob = X.e2j(user, f(), uid, rid);
        StringWriter writer = new StringWriter(2048);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeObject( ob.build() );
        return writer.toString();
    }



    @Override
    public String purchasesByUserInperiod(Date lo, Date hi, Long userid, Long uid, Long rid) {
        String result = null;
        if (lo == null || hi == null || userid == null) {
            return result;
        }
        result = "[]";
        if ( uid != null && !uid.equals(userid) ) {
            return result;
        }

        User user = _em.find(User.class, userid);
        if (user == null) {
            return result = null;
        }

        TreeMap<Date,JsonObjectBuilder> sorter = new TreeMap<>();
        Collection<Purchase> purchases = user.getPurchases();
        if (purchases != null) {
            for (Purchase purchase : purchases) {
                if (purchase == null) {
                    continue;
                }
                if (rid != null && !rid.equals( purchase.getResto().getId() )) {
                    continue;
                }
                Date ts = purchase.getTstamp();
                if (ts == null) {
                    continue;
                }
                if ( !lo.after(ts) && hi.after(ts) ) {
                    JsonObjectBuilder ob = X.e2j( purchase, f() );
                    ob.add( "user", user.getId() );
                    sorter.put(ts, ob);
                }
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add( ob );
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String purchasesFromRestoInperiod(Date lo, Date hi, String restoname, Long uid, Long rid) {
        String result = null;
        if (lo == null || hi == null) {
            return result;
        }
        result = "[]";
        if (restoname == null || restoname.length() <= 0 || lo.after(hi)) {
            return result;
        }

        Resto theResto = null;
        Query q = _em.createNamedQuery("findRestoByNama");
        q = q.setParameter("mynama", restoname);
        Collection<Resto> restos = q.getResultList();
        if (restos != null) {
            for (Resto resto : restos) {
                if (resto != null) {
                    theResto = resto;
                    break;
                }
            }
        }
        if (theResto == null) {
            return result = null;
        }
        if (rid != null && !rid.equals( theResto.getId() )) {
            return result;
        }

        Collection<User> users = null;
        if (uid != null) {
            User user = _em.find(User.class, uid);
            if (user == null) {
                return result;
            }
            users = new ArrayList<>();
            users.add( user );
        }
        else {
            q = _em.createNamedQuery( "tabscanUsers" );
            users = q.getResultList();
        }

        TreeMap<Date,JsonObjectBuilder> sorter = new TreeMap<>();
        if (users != null) {
            for (User user : users) {
                if (user == null) {
                    continue;
                }
                Collection<Purchase> purchases = user.getPurchases();
                if (purchases != null) {
                    for (Purchase purchase : purchases) {
                        if (purchase == null) {
                            continue;
                        }
                        Date ts = purchase.getTstamp();
                        if (ts == null) {
                            continue;
                        }
                        if ( !lo.after(ts) && hi.after(ts) &&  theResto.equals(purchase.getResto()) ) {
                            JsonObjectBuilder ob = X.e2j( purchase, f() );
                            ob.add( "user", user.getId() );
                            sorter.put(ts, ob);
                        }
                    }
                }
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add( ob );
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }




    @Override
    public String usersWithPurchaseAmountMoreOrLess(Date lo, Date hi, double lim) {
        String result = null;
        if (lo == null || hi == null) {
            return result;
        }
        result = "[]";
        if (lim == 0d || !lo.before(hi)) {
            return result;
        }

        Double top = null, bot = null;
        if (lim > 0d) {
            bot = lim;
        }
        else {
            top = -1d * lim;
        }

        TreeMap<Double,JsonObjectBuilder> sorter = new TreeMap<>();
        Query q = _em.createNamedQuery( "tabscanUsers" );
        Collection<User> users = q.getResultList();
        if (users != null) {
            for (User user : users) {
                if (user == null) {
                    continue;
                }
                double accu = 0d;
                Collection<Purchase> purchases = user.getPurchases();
                if (purchases != null) {
                    for (Purchase purchase : purchases) {
                        if (purchase == null) {
                            continue;
                        }
                        Date ts = purchase.getTstamp();
                        if (ts == null) {
                            continue;
                        }
                        if (!lo.after(ts) && hi.after(ts)) {
                            accu += purchase.getAmount();
                        }
                    }
                }
                if ((top != null && accu <= top) || (bot != null && accu >= bot)) {
                    JsonObjectBuilder ob = f().createObjectBuilder();
                    ob.add("id", user.getId());
                    ob.add("total-amount", String.format("%12.2f", accu).trim() );
                    double rev = (top != null) ? 1d : -1d;
                    Double key = new Double(rev * accu * 100000d   +   user.getId());
                    sorter.put(key, ob);
                }
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add( ob );
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String topRestosByPurchasesInperiod(Date lo, Date hi, int N, boolean numberamount) {
        String result = null;
        if (lo == null || hi == null) {
            return result;
        }
        result = "[]";
        if (N <= 0 || !lo.before(hi)) {
            return result;
        }

        HashMap<Long,Double> restoPurchases = new HashMap<>();
        Query q = _em.createNamedQuery( "tabscanUsers" );
        Collection<User> users = q.getResultList();
        if (users != null) {
            for (User user : users) {
                if (user == null) {
                    continue;
                }
                Collection<Purchase> purchases = user.getPurchases();
                if (purchases != null) {
                    for (Purchase purchase : purchases) {
                        if (purchase == null) {
                            continue;
                        }
                        Date ts = purchase.getTstamp();
                        if (ts == null) {
                            continue;
                        }
                        if (!lo.after(ts) && hi.after(ts)) {
                            Resto resto = purchase.getResto();
                            if (resto != null) {
                                Double sofar = restoPurchases.get( resto.getId() );
                                if (sofar == null) {
                                    sofar = new Double( numberamount ? purchase.getAmount() : 1d );
                                }
                                else {
                                    sofar += ( numberamount ? purchase.getAmount() : 1d );
                                }
                                restoPurchases.put( resto.getId(), sofar );
                            }
                        }
                    }
                }
            }
        }

        TreeMap<Double,JsonObjectBuilder> sorter = new TreeMap<>();
        for ( Long id : restoPurchases.keySet().toArray(new Long[0]) ) {
            Double val = restoPurchases.get(id);
            if (val != null) {

                JsonObjectBuilder ob = f().createObjectBuilder();
                ob.add("id", id);
                if (numberamount) {
                    ob.add( "total-amount-of-purchases", String.format("%12.2f", val).trim() );
                }
                else {
                    int num = val.intValue();
                    ob.add( "total-number-of-purchases", String.format("%7d", num).trim() );
                }
                Double key = new Double( -1d * val * 100000d   +   id );
                sorter.put(key, ob);
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        int n = 0;
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add(ob);
            ++n;
            if (n >= N) {
                break;
            }
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String topUsersByPurchaseAmount(Date lo, Date hi, int N) {
        String result = null;
        if (lo == null || hi == null) {
            return result;
        }
        result = "[]";
        if (N <= 0 || !lo.before(hi)) {
            return result;
        }
        TreeMap<Double,JsonObjectBuilder> sorter = new TreeMap<>();
        Query q = _em.createNamedQuery( "tabscanUsers" );
        Collection<User> users = q.getResultList();
        if (users != null) {
            for (User user : users) {
                if (user == null) {
                    continue;
                }
                double accu = 0d;
                Collection<Purchase> purchases = user.getPurchases();
                if (purchases != null) {
                    for (Purchase purchase : purchases) {
                        if (purchase == null) {
                            continue;
                        }
                        Date ts = purchase.getTstamp();
                        if (ts == null) {
                            continue;
                        }
                        if (!lo.after(ts) && hi.after(ts)) {
                            accu += purchase.getAmount();
                        }
                    }
                }
                JsonObjectBuilder ob = f().createObjectBuilder();
                ob.add("id", user.getId());
                ob.add("total-amount", String.format("%12.2f", accu).trim() );
                Double key = new Double(-1d * accu * 100000d   +   user.getId());
                sorter.put(key, ob);
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        int n = 0;
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add(ob);
            ++n;
            if (n >= N) {
                break;
            }
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String outletsWithDishesWithinPriceRange(double pmin, double pmax, long min, long max) {
        String result = null;
        if ( pmin > pmax || min > max ) {
            return result;
        }
        TreeMap<Long,JsonObjectBuilder> sorter = new TreeMap<>();
        Query q = _em.createNamedQuery( "tabscanOutlets" );
        Collection<Outlet> outlets = q.getResultList();
        if (outlets != null) {
            for (Outlet outlet : outlets) {
                if (outlet == null) {
                    continue;
                }
                long counter = 0L;
                Collection<MenuItem> dishes = outlet.getItems();
                if (dishes != null) {
                    for (MenuItem dish : dishes) {
                        if (dish == null) {
                            continue;
                        }
                        if (pmin <= dish.getPrice() && dish.getPrice() < pmax) {
                            ++counter;
                        }
                    }
                }
                if (min <= counter && counter < max) {
                    JsonObjectBuilder ob = f().createObjectBuilder();
                    ob.add("id", outlet.getId());
                    ob.add("number-of-dishes", counter);
                    Long key =
                        new Long(-1L * counter * 100000L   +   outlet.getId());
                    sorter.put(key, ob);
                }
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add(ob);
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String outletsByHoursPerweek(long min, long max) {
        String result = "[]";
        if (min > max) {
            return result;
        }
        TreeMap<Long,JsonObjectBuilder> sorter = new TreeMap<>();
        Query q = _em.createNamedQuery( "tabscanOutlets" );
        Collection<Outlet> outlets = q.getResultList();
        if (outlets != null) {
            for (Outlet outlet : outlets) {
                if (outlet == null) {
                    continue;
                }
                long milis = 0L;
                milis += (outlet.getSunh() - outlet.getSunl());
                milis += (outlet.getMonh() - outlet.getMonl());
                milis += (outlet.getTueh() - outlet.getTuel());
                milis += (outlet.getWedh() - outlet.getWedl());
                milis += (outlet.getThuh() - outlet.getThul());
                milis += (outlet.getFrih() - outlet.getFril());
                milis += (outlet.getSath() - outlet.getSatl());
                long hoursPerweek = milis / (1000L * 60L * 60L);
                if (min <= hoursPerweek && hoursPerweek < max) {

                    JsonObjectBuilder ob = f().createObjectBuilder();
                    ob.add("id", outlet.getId());
                    ob.add("total-hours-perweek", hoursPerweek);
                    Long key = new Long(-1L * hoursPerweek * 100000L   +   outlet.getId());
                    sorter.put(key, ob);
                }
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add(ob);
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String outletsByAvHoursPerday(long min, long max) {
        String result = "[]";
        if (min > max) {
            return result;
        }
        TreeMap<Long,JsonObjectBuilder> sorter = new TreeMap<>();
        Query q = _em.createNamedQuery( "tabscanOutlets" );
        Collection<Outlet> outlets = q.getResultList();
        if (outlets != null) {
            for (Outlet outlet : outlets) {
                if (outlet == null) {
                    continue;
                }
                long milis = 0L;
                milis += (outlet.getSunh() - outlet.getSunl());
                milis += (outlet.getMonh() - outlet.getMonl());
                milis += (outlet.getTueh() - outlet.getTuel());
                milis += (outlet.getWedh() - outlet.getWedl());
                milis += (outlet.getThuh() - outlet.getThul());
                milis += (outlet.getFrih() - outlet.getFril());
                milis += (outlet.getSath() - outlet.getSatl());
                long avperday = milis/7L;
                long hoursPerday = avperday / (1000L * 60L * 60L);
                if (min <= hoursPerday && hoursPerday < max) {

                    JsonObjectBuilder ob = f().createObjectBuilder();
                    ob.add("id", outlet.getId());
                    ob.add("average-hours-perday", hoursPerday);
                    Long key = new Long(-1L * hoursPerday * 100000L   +   outlet.getId());
                    sorter.put(key, ob);
                }
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add(ob);
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String searchNearbyOutlets(double x, double y, double distmax) {
        String result = null;
        distmax = Math.abs( distmax );
        if (distmax > .1d) {
            return result;
        }

        TreeMap<Double,JsonObjectBuilder> sorter = new TreeMap<>();
        Query q = _em.createNamedQuery( "tabscanOutlets" );
        Collection<Outlet> outlets = q.getResultList();
        if (outlets != null) {
            for (Outlet outlet : outlets) {
                if (outlet != null) {
                    double deltaX = outlet.getX() - x;
                    double deltaY = outlet.getY() - y;
                    double distance = Math.sqrt( deltaX * deltaX + deltaY * deltaY );
                    if (distance < distmax) {
                        JsonObjectBuilder ob = f().createObjectBuilder();
                        ob.add( "id", outlet.getId() );
                        String distancestr = String.format("%14.7e", distance).trim();
                        ob.add( "distance", distancestr);
                        double did = outlet.getId().doubleValue() * 1.e-12d;
                        Double key = distance + did;
                        sorter.put(key, ob);
                    }
                }
            }
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (JsonObjectBuilder ob : sorter.values()) {
            ab.add(ob);
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String searchOpenOutlets(Date then) {
        String result = "[]";
        if (then == null) {
            return result;
        }
        String qn = new String();
        cal().setTime(then);
        switch ( cal().get(Calendar.DAY_OF_WEEK) ) {
            case Calendar.SUNDAY:
                qn = "findSunOutlets";
                break;
            case Calendar.MONDAY:
                qn = "findMonOutlets";
                break;
            case Calendar.TUESDAY:
                qn = "findTueOutlets";
                break;
            case Calendar.WEDNESDAY:
                qn = "findWedOutlets";
                break;
            case Calendar.THURSDAY:
                qn = "findThuOutlets";
                break;
            case Calendar.FRIDAY:
                qn = "findFriOutlets";
                break;
            case Calendar.SATURDAY:
                qn = "findSatOutlets";
                break;
            default:
                break;
        }
        then = firstJan1970Second0Milis0(then);
        JsonArrayBuilder ab = f().createArrayBuilder();

        if (qn != null && qn.length() > 0) {
            Query q = _em.createNamedQuery( qn );
            q = q.setParameter( "then", then.getTime() );
            Collection<Outlet> outlets = q.getResultList();
            if (outlets != null) {
                for (Outlet outlet : outlets) {
                    if (outlet != null) {
                        ab.add( outlet.getId() );
                    }
                }
            }
        }
        StringWriter writer = new StringWriter(2048);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public String searchOutletsByKeyword(String keyword) {
        String result = "[]";
        if (keyword == null || keyword.length() < 4) {
            return result;
        }
        Long[] ids = null;
        try {
            ids = search().search(keyword);
        }
        catch (IOException ioe) {
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        if (ids != null) {
            for (Long id : ids) {
                if (id == null) {
                    continue;
                }
                ab.add(id);
            }
        }
        StringWriter writer = new StringWriter(2048);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray(ab.build());
        result = writer.toString();
        return result;
    }



    @Override
    public String fetchUsersWithIds(Long[] ids, Long uid, Long rid) {
        String result = null;
        if (ids == null) {
            return result;
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (Long id : ids) {
            if (id == null) {
                continue;
            }
            User user = _em.find(User.class, id);
            if (user != null) {
                JsonObjectBuilder ob = X.e2j(user, f(), uid, rid);
                if (ob != null) {
                    ab.add(ob);
                }
            }
        }
        StringWriter writer = new StringWriter(65536);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray(ab.build());
        result = writer.toString();
        return result;
    }



    @Override
    public String fetchOutletsWithIds(Long[] ids) {
        String result = null;
        if (ids == null) {
            return result;
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (Long id : ids) {
            if (id == null) {
                continue;
            }
            Outlet outlet = _em.find(Outlet.class, id);
            if (outlet != null) {
                JsonObjectBuilder ob = X.e2j(outlet, f());
                if (ob != null) {
                    ab.add(ob);
                }
            }
        }
        StringWriter writer = new StringWriter(65536);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray(ab.build());
        result = writer.toString();
        return result;
    }



    @Override
    public String fetchRestosWithIds(Long[] ids) {
        String result = null;
        if (ids == null) {
            return result;
        }
        JsonArrayBuilder ab = f().createArrayBuilder();
        for (Long id : ids) {
            if (id == null) {
                continue;
            }
            Resto resto = _em.find(Resto.class, id);
            if (resto != null) {
                JsonObjectBuilder ob = X.e2j(resto, f());
                if (ob != null) {
                    ab.add(ob);
                }
            }
        }
        StringWriter writer = new StringWriter(65536);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray(ab.build());
        result = writer.toString();
        return result;
    }



    @Override
    public String allRestos() {
        String result = "[]";
        JsonArrayBuilder ab = f().createArrayBuilder();

        Query q = _em.createNamedQuery( "tabscanResto" );
        Collection<Resto> restos = q.getResultList();
        if (restos != null) {
            for (Resto resto : restos) {
                if (resto == null) {
                    continue;
                }
                ab.add( resto.getId() );
            }
        }
        StringWriter writer = new StringWriter(4096);
        JsonWriter jwriter = Json.createWriter( writer );
        jwriter.writeArray( ab.build() );
        result = writer.toString();
        return result;
    }



    @Override
    public int doPopulateUser(String abspath) {
        int userStuffedIn = 0;
        try (FileInputStream fis = new FileInputStream(abspath)) {

            InputStreamReader reader = new InputStreamReader( fis );
            JsonReader jr = Json.createReader(reader);
            JsonStructure js = jr.read();
            if (js.getValueType() == JsonValue.ValueType.ARRAY) {

                JsonArray users = (JsonArray)js;
                int I = users.size();
                for (int i = 0; i < I; ++i) {
                    JsonObject user = users.getJsonObject(i);
                    Long id = updateUser(user);
                    if (id != null) {
                        userStuffedIn++;
                    }
                }
            }
        }
        catch (FileNotFoundException fnfe) {
        }
        catch (IOException ioe) {
        }
        return userStuffedIn;
    }



    @Override
    public int doPopulateResto(String abspath) {
        int restoStuffedIn = 0;
        try (FileInputStream fis = new FileInputStream(abspath)) {

            InputStreamReader reader = new InputStreamReader( fis );
            JsonReader jr = Json.createReader(reader);
            JsonStructure js = jr.read();
            if (js.getValueType() == JsonValue.ValueType.ARRAY) {
                JsonArray restos = (JsonArray)js;
                int I = restos.size();
                for (int i = 0; i < I; ++i) {
                    JsonObject resto = restos.getJsonObject(i);
                    Long id = updateOutlet(resto);
                    if (id != null) {
                        restoStuffedIn++;
                    }
                }
            }
        }
        catch (FileNotFoundException fnfe) {
        }
        catch (IOException ioe) {
        }
        return restoStuffedIn;
    }



    private Long updateUser(JsonObject rep) {
        Long result = null;
        if (rep == null) {
            return result;
        }
        final String empty = new String();

        String name = rep.getString("name", empty);
        if (name.length() <= 0) {
            return result;
        }

        Double x = null, y = null;
        String locs = rep.getString("location", empty);
        String[] loc = locs.split(",");
        if (loc != null && loc.length >= 2) {
            try {
                x = Double.parseDouble( loc[0] );
            }
            catch(NumberFormatException nfe) {
            }
            try {
                y = Double.parseDouble( loc[1] );
            }
            catch(NumberFormatException nfe) {
            }
        }
        if (x == null || y == null) {
            return result;
        }

        String uidstr = rep.getString("id", empty);
        Long uid = null;
        try {
            uid = Long.parseLong(uidstr);
        }
        catch(NumberFormatException nfe) {
        }
        User u = (uid != null) ? _em.find(User.class, uid) : new User();
        if (u == null) {
            return result;
        }
        u.setNama(name);
        u.setX(x);
        u.setY(y);

        String bals = rep.getString("balance", empty);
        Double bal = null;
        try {
            bal = Double.parseDouble(bals);
        }
        catch (NumberFormatException nfe) {
        }
        u.setBal(bal);

        Collection<Purchase> purliz = null;
        JsonArray purchases = rep.getJsonArray("purchases");
        if (purchases != null) {
            purliz = new ArrayList<>();

            int I = purchases.size();
            for (int i = 0; i < I; ++i) {
                JsonObject purchase = purchases.getJsonObject(i);
                if (purchase == null) {
                    continue;
                }

                String restoName = purchase.getString("restaurant_name", empty).trim();
                Resto theResto = null;
                Query q = _em.createNamedQuery("findRestoByNama");
                q = q.setParameter("mynama", restoName);
                Collection<Resto> restos = q.getResultList();
                if (restos != null) {
                    for (Resto resto : restos) {
                        if (resto != null) {
                            theResto = resto;
                            break;
                        }
                    }
                }
                if (theResto == null) {
                    continue;
                }

                Purchase p = new Purchase();
                p.setDish( purchase.getString("dish", empty) );
                p.setResto(theResto);

                String amounts = purchase.getString("amount", empty);
                try {
                    p.setAmount(Double.parseDouble(amounts));
                }
                catch(NumberFormatException nfe) {
                }

                String datestr = purchase.getString("date", empty);
                try {
                    p.setTstamp( sdf4().parse(datestr) );
                }
                catch(ParseException pe) {
                }
                purliz.add(p);
            }
        }
        u.setPurchases(purliz);
        if (uid == null) {
            _em.persist(u);
        }
        else {
            _em.merge(u);
        }
        result = u.getId();
        return result;
    }



    private Long updateOutlet(JsonObject rep) {
        Long result = null;
        if (rep == null) {
            return result;
        }
        final String empty = new String();

        String name = rep.getString("name", empty);
        if (name.length() <= 0) {
            return result;
        }

        Double x = null, y = null;
        String locs = rep.getString("location", empty);
        String[] loc = locs.split(",");
        if (loc != null && loc.length >= 2) {
            try {
                x = Double.parseDouble( loc[0] );
            }
            catch(NumberFormatException nfe) {
            }
            try {
                y = Double.parseDouble( loc[1] );
            }
            catch(NumberFormatException nfe) {
            }
        }
        if (x == null || y == null) {
            return result;
        }

        String ids = rep.getString("id", empty);
        Long oid = null;
        try {
            oid = Long.parseLong(ids);
        }
        catch(NumberFormatException nfe) {
        }
        Outlet o = (oid != null) ? _em.find(Outlet.class, oid) : new Outlet();
        if (o == null) {
            return result;
        }

        Resto theResto = null;
        Query q = _em.createNamedQuery("findRestoByNama");
        q = q.setParameter("mynama", name);
        Collection<Resto> restos = q.getResultList();
        if (restos != null) {
            for (Resto resto : restos) {
                if (resto != null) {
                    theResto = resto;
                    break;
                }
            }
        }
        if (theResto == null) {
            theResto = new Resto();
            theResto.setNama(name);
            _em.persist( theResto );
        }
        o.setResto(theResto);
        o.setX(x);
        o.setY(y);

        String bals = rep.getString("balance", empty);
        Double bal = null;
        try {
            bal = Double.parseDouble(bals);
        }
        catch (NumberFormatException nfe) {
        }
        o.setBal(bal);

        String hours = rep.getString("business_hours", empty);
        if (hours.length() > 0) {
            makeupBusinessHours(hours, o);
        }

        StringBuilder dishes = new StringBuilder();
        Collection<MenuItem> itemliz = null;
        JsonArray items = rep.getJsonArray("menu");
        if (items != null) {
            itemliz = new ArrayList<>();
            int I = items.size();
            for (int i = 0; i < I; ++i) {
                JsonObject item = items.getJsonObject(i);
                if (item == null) {
                    continue;
                }
                MenuItem mi = new MenuItem();
                mi.setNama(item.getString("name", empty));
                String prices = item.getString("price", empty);
                try {
                    mi.setPrice( Double.parseDouble(prices) );
                }
                catch(NumberFormatException nfe) {
                }
                if (mi.getNama().length() > 0 && mi.getPrice() > 0d) {
                    itemliz.add( mi );
                    if (dishes.length() > 0) {
                        dishes.append('|');
                    }
                    dishes.append(mi.getNama());
                }
            }
        }
        o.setItems(itemliz);

        if (oid == null) {
            _em.persist(o);
            try {
                index().addToIndex(o.getId(), o.getResto().getNama(), hours, dishes.toString());
            }
            catch(IOException ioe) {
            }
        }
        else {
            _em.merge(o);
            try {
                index().updateInIndex(o.getId(), o.getResto().getNama(), hours, dishes.toString());
            }
            catch(IOException ioe) {
            }
        }
        result = o.getId();
        return result;
    }


////////////////////////////////////////////////////////////////////////////////


    private void makeupBusinessHours(String hours, Outlet o) {
        if (hours == null || o == null) {
            return;
        }
        StringTokenizer tokenizer = new StringTokenizer(hours, "|", false);
        while ( tokenizer.hasMoreTokens() ) {
            String token = tokenizer.nextToken();
            if (token == null) {
                continue;
            }
            token = token.trim().toLowerCase();
            int idx = token.indexOf(':');
            if (idx < 0) {
                continue;
            }

            String days = null, range = null;
            days = token.substring(0, idx);
            idx += 1;
            if (idx < token.length()) {
                range = token.substring(idx, token.length());
            }
            if (days == null || range == null || days.length() <= 0 || range.length() <= 0) {
                continue;
            }

            days = makeCommaSeparated(days);

            Date lo = null, hi = null;
            String[] rng = range.split("\\-");
            if (rng != null && rng.length >= 2) {
                try {
                    lo = sdf().parse(rng[0]);
                }
                catch (ParseException pe) {
                    try {
                        lo = sdf2().parse(rng[0]);
                    }
                    catch (ParseException pe1) {
                        try {
                            lo = sdf3().parse(rng[0]);
                        }
                        catch (ParseException pe2) {
                        }
                    }
                }
                try {
                    hi = sdf().parse(rng[1]);
                }
                catch (ParseException pe) {
                    try {
                        hi = sdf2().parse(rng[1]);
                    }
                    catch (ParseException pe1) {
                        try {
                            hi = sdf3().parse(rng[1]);
                        }
                        catch (ParseException pe2) {
                        }
                    }
                }
            }
            if (lo == null || hi == null) {
                continue;
            }
            lo = firstJan1970Second0Milis0(lo);
            hi = firstJan1970Second0Milis0(hi);

            for (String key : _dow) {
                if ( !days.contains(key) ) {
                    continue;
                }
                switch (key) {
                    case "sun":
                        o.setSunl(lo.getTime());
                        if (lo.after(hi)) {
                            o.setSunh(eod().getTime());
                            o.setMonl(sod().getTime());
                            o.setMonh(hi.getTime());
                        }
                        else {
                            o.setSunh(hi.getTime());
                        }
                        break;
                    case "mon":
                        o.setMonl(lo.getTime());
                        if (lo.after(hi)) {
                            o.setMonh(eod().getTime());
                            o.setTuel(sod().getTime());
                            o.setTueh(hi.getTime());
                        }
                        else {
                            o.setMonh(hi.getTime());
                        }
                        break;
                    case "tue":
                        o.setTuel(lo.getTime());
                        if (lo.after(hi)) {
                            o.setTueh(eod().getTime());
                            o.setWedl(sod().getTime());
                            o.setWedh(hi.getTime());
                        }
                        else {
                            o.setTueh(hi.getTime());
                        }
                        break;
                    case "wed":
                        o.setWedl(lo.getTime());
                        if (lo.after(hi)) {
                            o.setWedh(eod().getTime());
                            o.setThul(sod().getTime());
                            o.setThuh(hi.getTime());
                        }
                        else {
                            o.setWedh(hi.getTime());
                        }
                        break;
                    case "thu":
                        o.setThul(lo.getTime());
                        if (lo.after(hi)) {
                            o.setThuh(eod().getTime());
                            o.setFril(sod().getTime());
                            o.setFrih(hi.getTime());
                        }
                        else {
                            o.setThuh(hi.getTime());
                        }
                        break;
                    case "fri":
                        o.setFril(lo.getTime());
                        if (lo.after(hi)) {
                            o.setFrih(eod().getTime());
                            o.setSatl(sod().getTime());
                            o.setSath(hi.getTime());
                        }
                        else {
                            o.setFrih(hi.getTime());
                        }
                        break;
                    case "sat":
                        o.setSatl(lo.getTime());
                        if (lo.after(hi)) {
                            o.setSath(eod().getTime());
                            o.setSunl(sod().getTime());
                            o.setSunh(hi.getTime());
                        }
                        else {
                            o.setSath(hi.getTime());
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }



    private String makeCommaSeparated( String range ) {
        if (range == null || !range.contains("-")) {
            return range;
        }
        String[] rng = range.split("\\-");
        if (rng == null || rng.length < 2) {
            return range;
        }
        rng[0] = rng[0].trim();
        rng[1] = rng[1].trim();
        int lo = -1, hi = -1;
        for (int i = 0; i < _dow.length; ++i) {
            if ( lo < 0 && rng[0].contains(_dow[i]) ) {
                lo = i;
            }
            if ( hi < 0 && rng[1].contains(_dow[i]) ) {
                hi = i;
            }
        }
        if (lo < 0 || hi < 0) {
            return range;
        }
        StringBuilder result = new StringBuilder();
        for (int i = lo; i != hi; i = ((i + 1) % _dow.length)) {
            if (result.length() > 0) {
                result.append(",");
            }
            result.append(_dow[i]);
        }
        if (result.length() > 0) {
            result.append(",");
        }
        result.append(_dow[hi]);
        return result.toString();
    }
    private String[] _dow = {"sun", "mon", "tue", "wed", "thu", "fri", "sat"};



    private Date firstJan1970Second0Milis0(Date d) {
        cal().setTime(d);
        cal().set(Calendar.YEAR, 1970);
        cal().set(Calendar.MONTH, 0);
        cal().set(Calendar.DATE, 1);
        cal().set(Calendar.SECOND, 0);
        cal().set(Calendar.MILLISECOND, 0);
        return cal().getTime();
    }



    private Calendar cal() {
        if (_cal == null) {
            _cal = Calendar.getInstance( getTimeZone() );
        }
        return _cal;
    }
    private Calendar _cal;


    private Date eod() {
        if (_eod == null) {
            _eod = firstJan1970Second0Milis0(new Date());
            cal().setTime(_eod);
            cal().set(Calendar.HOUR_OF_DAY, 23);
            cal().set(Calendar.MINUTE, 59);
            cal().set(Calendar.SECOND, 59);
            cal().set(Calendar.MILLISECOND, 999);
            _eod = cal().getTime();
        }
        return _eod;
    }
    private Date _eod;


    private Date sod() {
        if (_sod == null) {
            _sod = firstJan1970Second0Milis0(new Date());
            cal().setTime(_sod);
            cal().set(Calendar.HOUR_OF_DAY, 0);
            cal().set(Calendar.MINUTE, 0);
            _sod = cal().getTime();
        }
        return _sod;
    }
    private Date _sod;


    public TimeZone getTimeZone() {
        if (_tz == null) {
            _tz = TimeZone.getTimeZone("GMT+08");
        }
        return _tz;
    }
    private TimeZone _tz;



    private SimpleDateFormat sdf() {
        if (_sdf == null) {
            _sdf = new SimpleDateFormat("h:mm a");
            _sdf.setTimeZone(getTimeZone());
        }
        return _sdf;
    }
    private SimpleDateFormat _sdf;
    private SimpleDateFormat sdf2() {
        if (_sdf2 == null) {
            _sdf2 = new SimpleDateFormat("h a");
            _sdf2.setTimeZone(getTimeZone());
        }
        return _sdf2;
    }
    private SimpleDateFormat _sdf2;
    private SimpleDateFormat sdf3() {
        if (_sdf3 == null) {
            _sdf3 = new SimpleDateFormat("h.mm a");
            _sdf3.setTimeZone(getTimeZone());
        }
        return _sdf3;
    }
    private SimpleDateFormat _sdf3;
    private SimpleDateFormat sdf4() {
        if (_sdf4 == null) {
            _sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            _sdf4.setTimeZone(getTimeZone());
        }
        return _sdf4;
    }
    private SimpleDateFormat _sdf4;



    private OutletIndexer index() {
        if (_indexer == null) {
            _indexer = new OutletIndexer();
        }
        return _indexer;
    }
    private OutletIndexer _indexer;



    private OutletSearcher search() {
        if (_searcher == null) {
            _searcher = new OutletSearcher();
        }
        return _searcher;
    }
    private OutletSearcher _searcher;



    private JsonBuilderFactory f() {
        if (_builderFactory == null) {
            _builderFactory = Json.createBuilderFactory(null);
        }
        return _builderFactory;
    }
    private JsonBuilderFactory _builderFactory;
}
