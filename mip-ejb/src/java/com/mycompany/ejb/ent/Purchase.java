/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ent;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author bernarto
 */
@Embeddable
public class Purchase implements Serializable {
    public String getDish() {
        return _dish;
    }
    public void setDish(String dish) {
        _dish = dish;
    }
    private String _dish;


    public Resto getResto() {
        return _resto;
    }
    public void setResto(Resto resto) {
        _resto = resto;
    }
    private Resto _resto;


    public double getAmount() {
        return _amount;
    }
    public void setAmount(double amount) {
        _amount = amount;
    }
    private double _amount;


    @Temporal(TemporalType.DATE)
    public Date getTstamp() {
        return _tstamp;
    }
    public void setTstamp(Date tstamp) {
        _tstamp = tstamp;
    }
    private Date _tstamp;
}

