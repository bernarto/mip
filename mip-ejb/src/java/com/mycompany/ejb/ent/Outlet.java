/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import javax.persistence.ElementCollection;
import java.util.Collection;
import static javax.persistence.FetchType.EAGER;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author bernarto
 */
@Entity
@Table( name="MIP_OUTLET", uniqueConstraints = @UniqueConstraint(columnNames = {"RESTO_ID","X","Y"}) )
@NamedQueries({
    @NamedQuery(
        name="tabscanOutlets",
        query="SELECT o FROM Outlet o"),
    @NamedQuery(
        name="findOutletsOfResto",
        query="SELECT o FROM Outlet o WHERE o.resto = :myresto"),
    @NamedQuery(
        name="findSunOutlets",
        query="SELECT o FROM Outlet o WHERE o.sunl <= :then AND :then <= o.sunh"),
    @NamedQuery(
        name="findMonOutlets",
        query="SELECT o FROM Outlet o WHERE o.monl <= :then AND :then <= o.monh"),
    @NamedQuery(
        name="findTueOutlets",
        query="SELECT o FROM Outlet o WHERE o.tuel <= :then AND :then <= o.tueh"),
    @NamedQuery(
        name="findWedOutlets",
        query="SELECT o FROM Outlet o WHERE o.wedl <= :then AND :then <= o.wedh"),
    @NamedQuery(
        name="findThuOutlets",
        query="SELECT o FROM Outlet o WHERE o.thul <= :then AND :then <= o.thuh"),
    @NamedQuery(
        name="findFriOutlets",
        query="SELECT o FROM Outlet o WHERE o.fril <= :then AND :then <= o.frih"),
    @NamedQuery(
        name="findSatOutlets",
        query="SELECT o FROM Outlet o WHERE o.satl <= :then AND :then <= o.sath")
})
public class Outlet implements Serializable {
    private static final long serialVersionUID = 1L;


    @TableGenerator(
        name = "outletGen",
        table = "MIP_SEQUENCE_GENERATOR",
        pkColumnName = "GEN_KEY",
        valueColumnName = "GEN_VAL",
        pkColumnValue = "OUTLET-ID",
        allocationSize = 10)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "outletGen")
    public Long getId() {
        return _id;
    }
    public void setId(Long id) {
        _id = id;
    }
    protected Long _id;



    @NotNull
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name="RESTO_ID", referencedColumnName="ID")
    })
    public Resto getResto() {
        return _resto;
    }
    public void setResto(Resto resto) {
        _resto = resto;
    }
    private Resto _resto;



    @NotNull
    public Double getX() {
        return _x;
    }
    public void setX(Double x) {
        _x = x;
    }
    private Double _x;



    @NotNull
    public Double getY() {
        return _y;
    }
    public void setY(Double y) {
        _y = y;
    }
    private Double _y;



    public double getBal() {
        return _bal;
    }
    public void setBal(double bal) {
        _bal = bal;
    }
    private double _bal;



    public long getSunl() {
        return _sunl;
    }
    public void setSunl(long sunl) {
        _sunl = sunl;
    }
    private long _sunl;
    public long getSunh() {
        return _sunh;
    }
    public void setSunh(long sunh) {
        _sunh = sunh;
    }
    private long _sunh;



    public long getMonl() {
        return _monl;
    }
    public void setMonl(long monl) {
        _monl = monl;
    }
    private long _monl;
    public long getMonh() {
        return _monh;
    }
    public void setMonh(long monh) {
        _monh = monh;
    }
    private long _monh;



    public long getTuel() {
        return _tuel;
    }
    public void setTuel(long tuel) {
        _tuel = tuel;
    }
    private long _tuel;
    public long getTueh() {
        return _tueh;
    }
    public void setTueh(long tueh) {
        _tueh = tueh;
    }
    private long _tueh;



    public long getWedh() {
        return _wedh;
    }
    public void setWedh(long wedh) {
        _wedh = wedh;
    }
    private long _wedh;
    public long getWedl() {
        return _wedl;
    }
    public void setWedl(long wedl) {
        _wedl = wedl;
    }
    private long _wedl;



    public long getThuh() {
        return _thuh;
    }
    public void setThuh(long thuh) {
        _thuh = thuh;
    }
    private long _thuh;
    public long getThul() {
        return _thul;
    }
    public void setThul(long thul) {
        _thul = thul;
    }
    private long _thul;



    public long getFril() {
        return _fril;
    }
    public void setFril(long fril) {
        _fril = fril;
    }
    private long _fril;
    public long getFrih() {
        return _frih;
    }
    public void setFrih(long frih) {
        _frih = frih;
    }
    private long _frih;



    public long getSatl() {
        return _satl;
    }
    public void setSatl(long satl) {
        _satl = satl;
    }
    private long _satl;
    public long getSath() {
        return _sath;
    }
    public void setSath(long sath) {
        _sath = sath;
    }
    private long _sath;



    @ElementCollection(fetch=EAGER)
    public Collection<MenuItem> getItems() {
        return _items;
    }
    public void setItems(Collection<MenuItem> items) {
        _items = items;
    }
    private Collection<MenuItem> _items;



////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (_id != null) ? _id.hashCode() : 0;
        hash += (_resto != null) ? _resto.hashCode() : 0;
        hash += Double.hashCode(_x);
        hash += Double.hashCode(_y);
        return hash;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Outlet)) {
            return false;
        }
        Outlet rhs = (Outlet)o;

        return ((_id != null) ? _id.equals(rhs._id) : rhs._id == null)
                && (_x == rhs._x) && (_y == rhs._y)
                && ((_resto != null) ? _resto.equals(rhs._resto) : rhs._resto == null);
    }



    @Override
    public String toString() {
        return "com.mycompany.ejb.ent.Outlet[ id=" + _id + " ]";
    }
}

