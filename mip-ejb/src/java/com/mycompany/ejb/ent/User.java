/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;
import java.util.Collection;
import javax.persistence.ElementCollection;
import static javax.persistence.FetchType.EAGER;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author bernarto
 */
@Entity
@Table( name="MIP_USER", uniqueConstraints = @UniqueConstraint(columnNames = {"NAMA","X","Y"}) )
@NamedQueries({
    @NamedQuery(
        name="tabscanUsers",
        query="SELECT u FROM User u")
})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableGenerator(
        name = "userGen",
        table = "MIP_SEQUENCE_GENERATOR",
        pkColumnName = "GEN_KEY",
        valueColumnName = "GEN_VAL",
        pkColumnValue = "USER-ID",
        allocationSize = 10)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "userGen")
    public Long getId() {
        return _id;
    }
    public void setId(Long id) {
        _id = id;
    }
    protected Long _id;



    @NotNull
    public String getNama() {
        return _nama;
    }
    public void setNama(String nama) {
        _nama = nama;
    }
    private String _nama;



    @NotNull
    public double getX() {
        return _x;
    }
    public void setX(double x) {
        _x = x;
    }
    private double _x;



    @NotNull
    public double getY() {
        return _y;
    }
    public void setY(double y) {
        _y = y;
    }
    private double _y;



    public double getBal() {
        return _bal;
    }
    public void setBal(double bal) {
        _bal = bal;
    }
    private double _bal;



    @ElementCollection(fetch=EAGER)
    public Collection<Purchase> getPurchases() {
        return _purchases;
    }
    public void setPurchases(Collection<Purchase> purchases) {
        _purchases = purchases;
    }
    private Collection<Purchase> _purchases;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (_id != null ? _id.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        User rhs = (User)o;
        return ((_id != null) ? _id.equals(rhs._id) : rhs._id == null)
            && ((_nama != null) ? _nama.equals(rhs._nama) : rhs._nama == null);
    }


    @Override
    public String toString() {
        return "com.mycompany.ejb.ent.User[ id=" + _id + " ]";
    }
}
