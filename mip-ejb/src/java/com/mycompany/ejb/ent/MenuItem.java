/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ent;

import javax.persistence.Embeddable;
import java.io.Serializable;
/**
 *
 * @author bernarto
 */
@Embeddable
public class MenuItem implements Serializable {

    public String getNama() {
        return _name;
    }
    public void setNama(String nama) {
        _name = nama;
    }
    private String _name;


    public double getPrice() {
        return _price;
    }
    public void setPrice(double price) {
        _price = price;
    }
    private double _price;
}
