/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author bernarto
 */
@Entity
@Table(name = "MIP_RESTO", uniqueConstraints={ @UniqueConstraint(columnNames={"NAMA"}) })
@NamedQueries({
    @NamedQuery( name="findRestoByNama",
        query="SELECT r FROM Resto r WHERE r.nama = :mynama" ),
    @NamedQuery( name="tabscanResto",
        query="SELECT r FROM Resto r ORDER BY r.nama" )
})
public class Resto implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableGenerator(
        name = "restoGen",
        table = "MIP_SEQUENCE_GENERATOR",
        pkColumnName = "GEN_KEY",
        valueColumnName = "GEN_VAL",
        pkColumnValue = "RESTO-ID",
        allocationSize = 10)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "restoGen")
    public Long getId() {
        return _id;
    }
    public void setId(Long id) {
        _id = id;
    }
    protected Long _id;



    @NotNull
    public String getNama() {
        return _nama;
    }
    public void setNama(String nama) {
        _nama = nama;
    }
    private String _nama;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (_id != null ? _id.hashCode() : 0);
        hash += (_nama != null) ? _nama.hashCode() : 0;
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Resto)) {
            return false;
        }
        Resto rhs = (Resto)o;
        return ((_id != null) ? _id.equals(rhs._id) : rhs._id == null)
            && ((_nama != null) ? _nama.equals(rhs._nama) : rhs._nama == null);
    }


    @Override
    public String toString() {
        return "com.mycompany.ejb.ent.Resto[ id=" + _id + " ]";
    }
}
