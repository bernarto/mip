/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.backbean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import java.io.Serializable;
/**
 *
 * @author bernarto
 */

@Named
@SessionScoped
public class Dashboard implements Serializable {
    public String getWelcomeMessage() {
        return "Welcome to MIP";
    }    
}
