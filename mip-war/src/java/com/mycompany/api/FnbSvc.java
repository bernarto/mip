/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.api;

import javax.ejb.EJB;
import javax.ejb.EJBException;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import com.mycompany.ejb.ses.ControllerRemote;
import java.text.SimpleDateFormat;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;

import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.Date;

import java.text.ParseException;

/**
 *
 * @author bernarto
 */
@Path("fnb/{userid: [rRuU][0-9][0-9]*}")
public class FnbSvc {
    
    @EJB
    private ControllerRemote _controller;

    @Context
    private UriInfo _ctx;


    @Path("do-purchase")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response doPurchase (
            @DefaultValue("0") @FormParam("outletid") Long outletid,
            @DefaultValue("") @FormParam("itemname") String itemname,
            @PathParam("userid") String userid
    ) {
        if (outletid == null || outletid <= 0L) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"invalid outletid format\" }").build();
        }
        if (itemname.length() <= 0) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"itemname is required\" }").build();
        }

        Long uid = null, rid = null, eid = null;
        userid = userid.toLowerCase().trim();
        String idbuf = (userid.length() > 1) ? userid.substring(1) : new String();
        try {
            eid = Long.parseLong(idbuf);
        }
        catch (NumberFormatException nfe) {
        }
        if (eid == null) {
            return Response.status(Response.Status.UNAUTHORIZED)
                .entity("{ \"error-message\": \"fail to extract current user info\" }").build();
        }
        else if (userid.charAt(0) == 'u') {
            uid = eid;
        }
        else if (userid.charAt(0) == 'r') {
            rid = eid;
        }
        if (uid == null) {
            return Response.status(Response.Status.UNAUTHORIZED)
                .entity("{ \"error-message\": \"only user can execute a purchase, resto can not\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.doPurchase(outletid, itemname, uid, rid);
        }
        catch (EJBException ejbex) {
        }
        catch (IllegalArgumentException iae) {
            String msg = "{ \"error-message\": \"" + iae.getMessage() + "\" }";
            return Response.status(Response.Status.NOT_FOUND).entity(msg).build();
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("purchases-from-resto-inperiod")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response purchasesFromRestoInperiod (
            @DefaultValue("") @FormParam("lo") String lo,
            @DefaultValue("") @FormParam("hi") String hi,
            @DefaultValue("") @FormParam("restoname") String restoname,
            @PathParam("userid") String userid
    ) {
        Date dlo = null, dhi= null;
        try {
            dlo = sdf4().parse(lo);
            dhi = sdf4().parse(hi);
        }
        catch (ParseException pe) {
        }
        if (dlo == null || dhi == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"Both timestamps lo and hi are required and must be in 'yyyy-MM-dd HH:mm:ss z' format each\" }").build();
        }

        Long uid = null, rid = null, eid = null;
        userid = userid.toLowerCase().trim();
        String idbuf = (userid.length() > 1) ? userid.substring(1) : new String();
        try {
            eid = Long.parseLong(idbuf);
        }
        catch (NumberFormatException nfe) {
        }
        if (eid == null) {
            return Response.status(Response.Status.UNAUTHORIZED)
                .entity("{ \"error-message\": \"fail to extract current user info\" }").build();
        }
        else if (userid.charAt(0) == 'u') {
            uid = eid;
        }
        else if (userid.charAt(0) == 'r') {
            rid = eid;
        }

        String rep = null;
        try {
            rep = _controller.purchasesFromRestoInperiod(dlo, dhi, restoname, uid, rid);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.NOT_FOUND)
                .entity("{ \"error-message\": \"no such resto\" }").build();
        }
    }



    @Path("purchases-of-user-inperiod")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response purchasesOfUserInperiod (
            @DefaultValue("") @FormParam("lo") String lo,
            @DefaultValue("") @FormParam("hi") String hi,
            @DefaultValue("") @FormParam("id") String id,
            @PathParam("userid") String userid
    ) {
        Date dlo = null, dhi= null;
        try {
            dlo = sdf4().parse(lo);
            dhi = sdf4().parse(hi);
        }
        catch (ParseException pe) {
        }
        if (dlo == null || dhi == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"Both timestamps lo and hi are required and must be in 'yyyy-MM-dd HH:mm:ss z' format each\" }").build();
        }
        Long idl = null;
        try {
            idl = Long.parseLong(id);
        }
        catch (NumberFormatException nfe) {
        }
        if (idl == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"id of the user is required and must be a valid id of an existing user\" }").build();
        }

        Long uid = null, rid = null, eid = null;
        userid = userid.toLowerCase().trim();
        String idbuf = (userid.length() > 1) ? userid.substring(1) : new String();
        try {
            eid = Long.parseLong(idbuf);
        }
        catch (NumberFormatException nfe) {
        }
        if (eid == null) {
            return Response.status(Response.Status.UNAUTHORIZED)
                .entity("{ \"error-message\": \"fail to extract current user info\" }").build();
        }
        else if (userid.charAt(0) == 'u') {
            uid = eid;
        }
        else if (userid.charAt(0) == 'r') {
            rid = eid;
        }

        String rep = null;
        try {
            rep = _controller.purchasesByUserInperiod(dlo, dhi, idl, uid, rid);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.NOT_FOUND)
                .entity("{ \"error-message\": \"no such user\" }").build();
        }
    }



    @Path("users-with-totalpurchase-inperiod-morethanlimit")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response usersWithTotalpurchaseInperiodMorethanlimit (
            @DefaultValue("") @FormParam("lo") String lo,
            @DefaultValue("") @FormParam("hi") String hi,
            @DefaultValue("200") @FormParam("limit") Double limit,
            @PathParam("userid") String userid
    ) {
        Date dlo = null, dhi= null;
        try {
            dlo = sdf4().parse(lo);
            dhi = sdf4().parse(hi);
        }
        catch (ParseException pe) {
        }
        if (dlo == null || dhi == null || limit == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"Both timestamps lo and hi are required and must be in 'yyyy-MM-dd HH:mm:ss z' format each\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.usersWithPurchaseAmountMoreOrLess(dlo, dhi, limit);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("top-n-restos-in-purchase-number-inperiod")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response topNrestosInPurchaseNumberInperiod (
            @DefaultValue("10") @FormParam("N") Integer N,
            @DefaultValue("") @FormParam("lo") String lo,
            @DefaultValue("") @FormParam("hi") String hi,
            @PathParam("userid") String userid
    ) {
        Date dlo = null, dhi= null;
        try {
            dlo = sdf4().parse(lo);
            dhi = sdf4().parse(hi);
        }
        catch (ParseException pe) {
        }
        if (dlo == null || dhi == null || N == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"Both timestamps lo and hi are required and must be in 'yyyy-MM-dd HH:mm:ss z' format each\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.topRestosByPurchasesInperiod(dlo, dhi, N, false);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("top-n-restos-in-purchase-amount-inperiod")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response topNrestosInPurchaseAmountInperiod (
            @DefaultValue("10") @FormParam("N") Integer N,
            @DefaultValue("") @FormParam("lo") String lo,
            @DefaultValue("") @FormParam("hi") String hi,
            @PathParam("userid") String userid
    ) {
        Date dlo = null, dhi= null;
        try {
            dlo = sdf4().parse(lo);
            dhi = sdf4().parse(hi);
        }
        catch (ParseException pe) {
        }
        if (dlo == null || dhi == null || N == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"Both timestamps lo and hi are required and must be in 'yyyy-MM-dd HH:mm:ss z' format each\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.topRestosByPurchasesInperiod(dlo, dhi, N, true);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("top-n-users-in-purchase-amount-inperiod")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response topNusersInPurchaseAmountInperiod (
            @DefaultValue("10") @FormParam("N") Integer N,
            @DefaultValue("") @FormParam("lo") String lo,
            @DefaultValue("") @FormParam("hi") String hi,
            @PathParam("userid") String userid
    ) {
        Date dlo = null, dhi= null;
        try {
            dlo = sdf4().parse(lo);
            dhi = sdf4().parse(hi);
        }
        catch (ParseException pe) {
        }
        if (dlo == null || dhi == null || N == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"Both timestamps lo and hi are required and must be in 'yyyy-MM-dd HH:mm:ss z' format each\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.topUsersByPurchaseAmount(dlo, dhi, N);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("outlets-with-dishes-in-price-range")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response outletsWithDishesInPriceRange (
            @DefaultValue("12,14") @FormParam("priceminmax") String priceminmax,
            @DefaultValue("3,7") @FormParam("dishminmax") String dishminmax,
            @PathParam("userid") String userid
    ) {
        Double pmin = null, pmax = null;
        String[] dbuf = priceminmax.split(",");
        if (dbuf != null && dbuf.length >= 2) {
            try {
                pmin = Double.parseDouble( dbuf[0] );
                pmax = Double.parseDouble( dbuf[1] );
            }
            catch (NumberFormatException nfe) {
            }
        }
        Long min = null, max = null;
        String[] lohi = dishminmax.split(",");
        if (lohi != null && lohi.length >= 2) {
            try {
                min = Long.parseLong(lohi[0] );
                max = Long.parseLong(lohi[1] );
            }
            catch (NumberFormatException nfe) {
            }
        }
        if (pmin == null || pmax == null || min == null || max == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"invalid parameters\" }").build();
        }

        String rep = null;
        try {
            rep = _controller.outletsWithDishesWithinPriceRange(pmin, pmax, min, max);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("outlets-by-hoursperweek")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response outletsByHoursPerweek (
            @DefaultValue("80,90") @FormParam("hoursminmax") String hoursminmax,
            @PathParam("userid") String userid
    ) {
        Long min = null, max = null;
        String[] lohi = hoursminmax.split(",");
        if (lohi != null && lohi.length >= 2) {
            try {
                min = Long.parseLong( lohi[0] );
                max = Long.parseLong( lohi[1] );
            }
            catch (NumberFormatException nfe) {
            }
        }
        if (min == null || max == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"invalid parameters\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.outletsByHoursPerweek(min, max);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("outlets-by-avhoursperday")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response outletsByAvHoursPerday (
            @DefaultValue("13,17") @FormParam("hoursminmax") String hoursminmax,
            @PathParam("userid") String userid
    ) {
        Long min = null, max = null;
        String[] lohi = hoursminmax.split(",");
        if (lohi != null && lohi.length >= 2) {
            try {
                min = Long.parseLong( lohi[0] );
                max = Long.parseLong( lohi[1] );
            }
            catch (NumberFormatException nfe) {
            }
        }
        if (min == null || max == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"invalid parameters\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.outletsByAvHoursPerday(min, max);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("outlets-nearby")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response outletsNearby (
            @DefaultValue("0,0") @FormParam("location") String location,
            @DefaultValue("1.E-2") @FormParam("distmax") Double distmax,
            @PathParam("userid") String userid
    ) {
        Double x = null, y = null;
        String[] loc = location.split(",");
        if (loc != null && loc.length >= 2) {
            try {
                x = Double.parseDouble( loc[0] );
                y = Double.parseDouble( loc[1] );
            }
            catch (NumberFormatException nfe) {
            }
        }
        if (x == null || y == null || distmax == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"invalid parameters\" }").build();
        }
        if (distmax > .1d) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"distmax must be smaller than 1.E-1\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.searchNearbyOutlets(x, y, distmax);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("outlets-open-at")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response outletsOpenAt (
            @DefaultValue("") @FormParam("ts") String ts,
            @PathParam("userid") String userid
    ) {
        Date then = null;
        try {
            then = sdf4().parse(ts);
        }
        catch (ParseException pe) {
        }
        if (then == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"Timestamp ts must be in 'yyyy-MM-dd HH:mm:ss z' format\" }").build();
        }
        String rep = null;
        try {
            rep = _controller.searchOpenOutlets(then);
        }
        catch (EJBException ejbex) {
        }
        if (rep != null) {
            return Response.status( Response.Status.OK ).entity(rep).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("outlets-by-keyword")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response outletsByKeyword (
            @DefaultValue("") @FormParam("keyword") String keyword,
            @PathParam("userid") String userid
    ) {
        if (keyword.length() < 4) {
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("{ \"error-message\": \"keyword must consist of 4 chars or more\" }").build();
        }
        String sr = null;
        try {
            sr = _controller.searchOutletsByKeyword(keyword);
        }
        catch (EJBException ejbex) {
        }
        if (sr != null) {
            return Response.status( Response.Status.OK ).entity(sr).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("all-restos")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response allRestos (
            @PathParam("userid") String userid
    ) {
        String payload = null;
        try {
            payload = _controller.allRestos();
        }
        catch (EJBException ejbex) {
        }
        if (payload != null) {
            return Response.status( Response.Status.OK ).entity(payload).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("grab-restos-by-ids")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response grabRestosByIds (
            @DefaultValue("") @FormParam("ids") String idstr,
            @PathParam("userid") String userid
    ) {
        idstr = idstr.trim();
        if (idstr.length() > 1 && idstr.charAt(0) == '[') {
            idstr = idstr.substring(1);
        }
        if (idstr.length() > 1 && idstr.charAt(idstr.length() - 1) == ']') {
            idstr = idstr.substring(0, idstr.length() - 1);
        }

        ArrayList<Long> idliz = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer( idstr, ",", false );
        while (tokenizer.hasMoreTokens()) {

            String token = tokenizer.nextToken();
            Long id = null;
            try {
                id = Long.parseLong(token);
            }
            catch (NumberFormatException nfe) {
            }
            if (id != null) {
                idliz.add(id);
            }
        }
        Long[] ids = idliz.toArray( new Long[ 0 ] );
        String payload = null;
        try {
            payload = _controller.fetchRestosWithIds( ids );
        }
        catch (EJBException ejbex) {
        }
        if (payload != null) {
            return Response.status( Response.Status.OK ).entity(payload).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("grab-outlets-by-ids")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response grabOutletsByIds (
            @DefaultValue("") @FormParam("ids") String idstr,
            @PathParam("userid") String userid
    ) {
        idstr = idstr.trim();
        if (idstr.length() > 1 && idstr.charAt(0) == '[') {
            idstr = idstr.substring(1);
        }
        if (idstr.length() > 1 && idstr.charAt(idstr.length() - 1) == ']') {
            idstr = idstr.substring(0, idstr.length() - 1);
        }

        ArrayList<Long> idliz = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer( idstr, ",", false );
        while (tokenizer.hasMoreTokens()) {

            String token = tokenizer.nextToken();
            Long id = null;
            try {
                id = Long.parseLong(token);
            }
            catch (NumberFormatException nfe) {
            }
            if (id != null) {
                idliz.add(id);
            }
        }
        Long[] ids = idliz.toArray( new Long[ 0 ] );
        String payload = null;
        try {
            payload = _controller.fetchOutletsWithIds( ids );
        }
        catch (EJBException ejbex) {
        }
        if (payload != null) {
            return Response.status( Response.Status.OK ).entity(payload).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }



    @Path("grab-users-by-ids")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response grabUsersByIds (
            @DefaultValue("") @FormParam("ids") String idstr,
            @PathParam("userid") String userid
    ) {
        idstr = idstr.trim();
        if (idstr.length() > 1 && idstr.charAt(0) == '[') {
            idstr = idstr.substring(1);
        }
        if (idstr.length() > 1 && idstr.charAt(idstr.length() - 1) == ']') {
            idstr = idstr.substring(0, idstr.length() - 1);
        }

        ArrayList<Long> idliz = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer( idstr, ",", false );
        while (tokenizer.hasMoreTokens()) {

            String token = tokenizer.nextToken();
            Long id = null;
            try {
                id = Long.parseLong(token);
            }
            catch (NumberFormatException nfe) {
            }
            if (id != null) {
                idliz.add(id);
            }
        }
        Long[] ids = idliz.toArray( new Long[ 0 ] );

        Long uid = null, rid = null, eid = null;
        userid = userid.toLowerCase().trim();
        String idbuf = (userid.length() > 1) ? userid.substring(1) : new String();
        try {
            eid = Long.parseLong(idbuf);
        }
        catch (NumberFormatException nfe) {
        }
        if (eid == null) {
            return Response.status(Response.Status.UNAUTHORIZED)
                .entity("{ \"error-message\": \"fail to extract current user info\" }").build();
        }
        else if (userid.charAt(0) == 'u') {
            uid = eid;
        }
        else if (userid.charAt(0) == 'r') {
            rid = eid;
        }

        String payload = null;
        try {
            payload = _controller.fetchUsersWithIds( ids, uid, rid );
        }
        catch (EJBException ejbex) {
        }
        if (payload != null) {
            return Response.status( Response.Status.OK ).entity(payload).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{ \"error-message\": \"unknown internal server problem\" }").build();
        }
    }


////////////////////////////////////////////////////////////////////////////////


    public TimeZone getTimeZone() {
        if (_tz == null) {
            _tz = TimeZone.getTimeZone("GMT+08");
        }
        return _tz;
    }
    private TimeZone _tz;


    public SimpleDateFormat sdf4() {
        if (_sdf4 == null) {
            _sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            _sdf4.setTimeZone(getTimeZone());
        }
        return _sdf4;
    }
    private SimpleDateFormat _sdf4;
}



