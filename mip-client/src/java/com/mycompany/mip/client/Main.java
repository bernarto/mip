package com.mycompany.mip.client;

import com.mycompany.ejb.ses.ControllerRemote;
import java.io.InputStreamReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;



import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonReader;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 * Enterprise Application Client main class.
 *
 */
public class Main {

    @EJB
    private static ControllerRemote _controller;

    public static void main( String[] args ) {
        if (args.length < 1) {
            System.err.println("Usage: <switch: resto/user/test1>  [<absolute file path of the input json file>]");
            System.exit(1);
        }
        if (args.length >= 2 && args[0].toLowerCase().contains("user")) {
            int numberOfUsers = _controller.doPopulateUser(args[1]);
            System.out.println("Number of users stuffed in : " + numberOfUsers);
        }
        else if (args.length >= 2 && args[0].toLowerCase().contains("resto")) {
            int numberOfRestos = _controller.doPopulateResto(args[1]);
            System.out.println("Number of restos stuffed in : " + numberOfRestos);
        }
        else if (args.length >= 1 && args[0].toLowerCase().contains("test1")) {
            Long[] ids = {1L, 2L, 3L};
            String buf = _controller.fetchOutletsWithIds(ids);
            String dbg = buf;
        }
        else if (args.length >= 1 && args[0].toLowerCase().contains("test2")) {
            Long[] ids = {1L, 2L, 3L};
            String buf = _controller.fetchUsersWithIds(ids, null, null);
            String dbg = buf;
        }
        else if (args.length >= 1 && args[0].toLowerCase().contains("test3")) {
            String keyword = "resto:house";
            String buf = _controller.searchOutletsByKeyword(keyword);
            String dbg = buf;
        }
        else {
            System.err.println("Invalid parameters");
        }
    }
}


/*
    public static void main( String[] args ) {
        try (FileInputStream fis = new FileInputStream("restaurants.json")) {

            InputStreamReader reader = new InputStreamReader( fis );
            JsonReader jr = Json.createReader(reader);
            JsonStructure js = jr.read();
            if (js.getValueType() == JsonValue.ValueType.ARRAY) {
                JsonArray restos = (JsonArray)js;
                int I = restos.size();
                for (int i = 0; i < I; ++i) {
                    JsonObject resto = restos.getJsonObject(i);
                    Long id = _controller.updateOutlet(resto);
                    if (id == null) {
                        System.err.println("Fail to stuff in resto:" + resto.getString("name") + " | location:" + resto.getString("location"));
                        break;
                    }
                }
            }
        }
        catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
*/
