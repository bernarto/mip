/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb.ses;

import java.util.Date;
import javax.ejb.Remote;

/**
 *
 * @author bernarto
 */
@Remote
public interface ControllerRemote {

    int doPopulateResto(String abspath);

    int doPopulateUser(String abspath);

    String fetchOutletsWithIds(Long[] ids);

    String fetchUsersWithIds(Long[] ids, Long uid, Long rid);

    String searchOutletsByKeyword(String keyword);

    String searchOpenOutlets(Date then);

    String searchNearbyOutlets(double x, double y, double distmax);

    String outletsByHoursPerweek(long min, long max);

    String outletsByAvHoursPerday(long min, long max);

    String outletsWithDishesWithinPriceRange(double pmin, double pmax, long min, long max);

    String topUsersByPurchaseAmount(Date lo, Date hi, int n);

    String topRestosByPurchasesInperiod(Date lo, Date hi, int N, boolean numberamount);

    String usersWithPurchaseAmountMoreOrLess(Date lo, Date hi, double lim);

    String purchasesFromRestoInperiod(Date lo, Date hi, String restoname, Long uid, Long rid);

    String purchasesByUserInperiod(Date lo, Date hi, Long userid, Long uid, Long rid);

    String doPurchase(Long oid, String dish, Long uid, Long rid);

    String fetchRestosWithIds(Long[] ids);

    String allRestos();
}
