HOW TO SETUP AND START THE BACKEND SERVER

You can setup and run this on any OS/platform of your choice.
For now Windows/MacOS/Linux/Solaris


FOR PROD:

1. Download and install JDK 1.8 from oracle.com (preferably the latest build)

2. Download and instal glassfish 4.x from glassfish.org 

3. Set env var JAVA_HOME, AS_HOME to point to your JDK and glassfish instalation

4. Set PATH to include the bin subdir of JAVA_HOME and AS_HOME

5. Start domain1 on glassfish, run command
      prompt> asadmin start-domain domain1

6. Start database on glassfish, run command
      prompt asadmin start-database

7. Create an empty subdir for example /root/stub

8. Check and makesure glassfish contains no application deployed, run command
      prompt> asadmin list-applications

9. Deploy file mip.ear, run command
      prompt> asadmin deploy /retrieve /root/stub mip.ear

      (pls note: /root/stub is the subdir we created above)
      (this command will take sometime, pls be patient)


10. Check and makesure that mip.ear deployed successfuly, run command
      prompt> asadmin list-applications
      (also check the content of subdir /root/stub created above)


11. Populate with restaurant.json first before users.json, run command
      prompt> appclient -client /root/stub/mipClient.jar -mainclass com.mycompany.mip.client.Main -targetserver localhost:3700 resto /root/restaurants.json

      (this command will take sometime, pls be patient)
      (the path of file restaurants.json must be given as an absolute path)


12. Only after the previous step successful, populate with users.json, run command
      prompt> appclient -client /root/stub/mipClient.jar -mainclass com.mycompany.mip.client.Main -targetserver localhost:3700 user /root/users.json

      (this command will take sometime, pls be patient)
      (the path of file users.json must be given as an absolute path)
     

13. Perform a quick check to get the id of all restos, run command
      prompt> curl -d post=1  http://localhost:8080/mip-war/api/fnb/u22/all-restos

14. Check API doc for the other api request you can perform



FOR DEV:

1. Download and instal JDK 1.8 from oracle.com

2. Download and instal netbeans 4.x that bundles together with glassfish 4.x

3. Clone this repo, run command
       prompt> git clone https://bernarto@bitbucket.org/bernarto/mip.git

4. Launch netbeans and open project mip

5. Open all the modules mip-ejb, mip-war, mip-client

6. Open project inside the mip subdir namely mip-stub

7. Do a build of the whole mip project
       right click on the triangle icon, select build

8. Start glassfish in the debug mode
      (this command will take sometime, pls be patient)

9. Debug the mip project
      right click on the triangle icon, select debug
      (this command will take sometime, pls be patient)
      (you will see it deploying the binary mip.ear and running it)
      (finaly you see your browser is launched for the welcome message)

10. You can put break points, single steping and watch variables values
       and alike

11. To populate with restaurants.json, right click->properties->run tab
      on the mip-client project icon
      type in the argument field:
              "resto  <absolute-path-to-file-restaurants.json>"

12. The same things with users.json

13. Debug it, by right click mip-client icon->select debug
      (this command will take sometime, pls be patient)

14. When finish, select menu: Debug->Finish debug

15. right click on the glassfish icon, expand applications and undeploy mip

16. Shutdown glassfish by right click its icon -> stop

17. Shutdown database by right click its icon -> stop

